const express       = require('express');
const app           = express();
const bodyParser    = require('body-parser'); /* to parse the request output */
app.use(bodyParser.urlencoded({extended:false}));

app.set('view engine', 'pug');


const routes = require('./routes');
app.use('/',routes);

/* Handling error when user inputs wrong url */
app.use(( req, res, next)=>{
    const error = new Error('Not Found');
    error.status=404;
    next(error);
});

app.use((err,req,res,next)=>{
    res.locals.error=err;
    res.status(err.status);
    res.render('error');
    setTimeout(function(){
        // res.redirect('/sort'); /* Redirecting to sort page after delay of 3s */
    }, 3000);
});

app.listen(process.env.PORT || 4000);