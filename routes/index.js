const express = require('express');
const router = express.Router(); 
const bodyParser    = require('body-parser'); /* middleware to parse the request output */

router.use(bodyParser.urlencoded({extended:false}));

/* Main business logic */
function blogic(input){
    let freq = {};
    let arr = [];

    try{

        /* converting string to JSON object */
        input = JSON.parse(input);

        /* calculating frequency of each and every element using map */
        input.map(value =>{
            if(value in freq){
                freq[value]=freq[value]+1;
            }
            else{
                freq[value] = 1;
            }
        });

        /* converting json object to 2D array */
        for(val in freq){
            arr.push([val,freq[val]])
        }

        /* sort on the basics of values */
        arr.sort(function(a,b){ 
            return b[1]-a[1];
        });

        let txt1 = [];  
        for(let i=0,y;y=arr[i];++i){
            let txt=y[0]+' is '+y[1];
            if(y[1]=='1'){
                txt+=' time';
            }
            else{
                txt+=' times';
            }
            txt1.push(txt); // converting to string of arrays
        }
        return txt1;
    } catch(error){
        return null;
    }
}


/* redirect root to '/sort' page */
router.get('/', (req,res)=>{
    res.redirect('/sort');
});

/* sort page */
router.get('/sort', (req,res) => {
    res.render('index',{error:''});
})

/* when user inputs the data in the input box */
router.post('/sort', (req,res)=>{
    let output = [];
    output = blogic(req.body.array);
    if(output)
        // res.send(output)
        {res.render('index',{output});}
    else{
        res.render('index',{error:'Wrong Format'}); //Wrong format entered
    }
});

/* when user inputs the array in the url itself (format: localhost:4000/sort/[1,2,3,...]) */
router.get('/sort/:id', (req,res)=>{
    let output = [];
    output = blogic(req.params.id);

    if(output)
        res.render('index',{output});
    else{
        res.render('index',{error:'Wrong Format'}); //Wrong format entered
    }
});


module.exports = router;